Static pages app
================

.. automodule:: pdp.pages
    :members:

Models
------

.. automodule:: pdp.pages.models
    :members:

Views
-----

.. automodule:: pdp.pages.views
    :members:
